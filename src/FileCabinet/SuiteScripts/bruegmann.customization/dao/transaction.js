define(
    ['N/record', 'N/search', '../common/constants'],
    function (record, search, constants) {

        function getDepositJournalForReversal (salesOrderId, invoiceAmount) {
            let depositSearch = search.create({
                type: search.Type.CUSTOMER_DEPOSIT,
                filters:
                [
                    ['salesorder', 'anyof', salesOrderId],
                    'AND',
                    ['totalamount', 'lessthanorequalto', invoiceAmount],
                    'AND',
                    ['custbody_pdg_vat_correction_je', 'noneof', '@NONE@']
                ],
                columns: [
                    constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CORRECTION_JOURNAL
                ]
            });

            let journalId = null;

            depositSearch.run().each(function(result) {
                journalId = result.getValue(constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CORRECTION_JOURNAL);
            });

            return journalId;
        }

        function getPrepaymentJournalForReversal (purchaseOrderId, billAmount) {
            let prepaymentSearch = search.create({
                type: search.Type.VENDOR_PREPAYMENT,
                filters:
                    [
                        ['appliedtotransaction', 'anyof', purchaseOrderId],
                        'AND',
                        ['totalamount', 'greaterthanorequalto', (billAmount * -1)],
                        'AND',
                        ['custbody_pdg_vat_correction_je', 'noneof', '@NONE@']
                    ],
                columns: [
                    constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CORRECTION_JOURNAL
                ]
            });

            let journalId = null;

            prepaymentSearch.run().each(function(result) {
                journalId = result.getValue(constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CORRECTION_JOURNAL);
            });

            return journalId;
        }

        function getCreditAccountFromDeposit (depositId) {
            let depositSearch = search.create({
                type: search.Type.CUSTOMER_DEPOSIT,
                filters: [
                    ['internalid', 'anyof', depositId],
                    'AND',
                    ['creditamount', 'isnotempty', '']
                ],
                columns: [
                    'account'
                ]
            });

            let creditAccount;

            depositSearch.run().each(function(result) {
                creditAccount = result.getValue('account');
            });

            return creditAccount;
        }

        function getDebitAccountFromPrepayment (depositId) {
            let prepaymentSearch = search.create({
                type: search.Type.VENDOR_PREPAYMENT,
                filters: [
                    ['internalid', 'anyof', depositId],
                    'AND',
                    ['debitamount', 'isnotempty', '']
                ],
                columns: [
                    'account'
                ]
            });

            let debitAccount;

            prepaymentSearch.run().each(function(result) {
                debitAccount = result.getValue('account');
            });

            return debitAccount;
        }

        function getUpdatedTransactionNumber (transactionId) {
            let transactionObj = search.lookupFields({
                type: search.Type.TRANSACTION,
                id: transactionId,
                columns: ['tranid']
            });

            return transactionObj.tranid;
        }

        function getJournalIdFromVendorPrepayment (vendorPrepaymentId) {
            let transactionObj = search.lookupFields({
                type: search.Type.VENDOR_PREPAYMENT,
                id: vendorPrepaymentId,
                columns: [constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CORRECTION_JOURNAL]
            });

            if (transactionObj[constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CORRECTION_JOURNAL] &&
                transactionObj[constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CORRECTION_JOURNAL][0]) {
                return transactionObj[constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CORRECTION_JOURNAL][0].value;
            }
            return null;
        }

        function getPurchaseOrderIDFromVendorBill(vendorBillId) {
            let vendorBill = record.load({
               type: record.Type.VENDOR_BILL,
               id: vendorBillId,
            });

            if (vendorBill.getLineCount('purchaseorders') > 0) {
                return vendorBill.getSublistValue({
                    sublistId: 'purchaseorders',
                    fieldId: 'id',
                    line: 0
                });
            }

            return null;
        }

        return {
            getDepositJournalForReversal,
            getPrepaymentJournalForReversal,
            getCreditAccountFromDeposit,
            getDebitAccountFromPrepayment,
            getUpdatedTransactionNumber,
            getJournalIdFromVendorPrepayment,
            getPurchaseOrderIDFromVendorBill,
        }
    });