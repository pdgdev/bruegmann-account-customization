/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/currentRecord', 'N/runtime', '../common/constants'],
/**
 * @param{currentRecord} currentRecord
 * @param{runtime} runtime
 */
function(currentRecord, runtime, constants) {

    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext) {
        let scriptObj = runtime.getCurrentScript();
        let germanSubsidiaries = scriptObj.getParameter(constants.SCRIPT_PARAMS.CLIENT_SCRIPT.GERMAN_SUBSIDIARIES);

        if (germanSubsidiaries) {
            let germanSubsidiariesArray = germanSubsidiaries.split(',');
            let subsidiary = scriptContext.currentRecord.getValue('subsidiary');
            let VATCodeField = scriptContext.currentRecord.getField(constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CODE);

            if (VATCodeField && germanSubsidiariesArray.indexOf(subsidiary) !== -1) {
                VATCodeField.isDisabled = false;
            }
        }
    }

    /**
     * Function to be executed when field is slaved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     *
     * @since 2015.2
     */
    function postSourcing(scriptContext) {
        if (scriptContext.fieldId !== 'customer' && scriptContext.fieldId !== 'entity' && scriptContext.fieldId !== 'subsidiary') {
            return;
        }

        let scriptObj = runtime.getCurrentScript();
        let germanSubsidiaries = scriptObj.getParameter(constants.SCRIPT_PARAMS.CLIENT_SCRIPT.GERMAN_SUBSIDIARIES);
        let VATCodeField = scriptContext.currentRecord.getField(constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CODE);

        if (VATCodeField && germanSubsidiaries) {
            let germanSubsidiariesArray = germanSubsidiaries.split(',');
            let subsidiary = scriptContext.currentRecord.getValue('subsidiary');

            if (germanSubsidiariesArray.indexOf(subsidiary) !== -1) {
                VATCodeField.isDisabled = false;
            }
            else {
                scriptContext.currentRecord.setValue({
                    fieldId: constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CODE,
                    value: ''
                });

                VATCodeField.isDisabled = true;
            }
        }
    }

    return {
        pageInit: pageInit,
        postSourcing: postSourcing,
    };
    
});
