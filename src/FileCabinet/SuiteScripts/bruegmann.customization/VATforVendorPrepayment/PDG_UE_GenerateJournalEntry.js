/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 */
define(['N/record', 'N/runtime', './journalBuilder', '../common/constants', '../dao/transaction'],
    /**
 * @param{record} record
 */
    (record, runtime, journalBuilder, constants, transactionDAO) => {

        /**
         * Defines the function definition that is executed after record is submitted.
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type; use values from the context.UserEventType enum
         * @since 2015.2
         */
        const afterSubmit = (scriptContext) => {
            let transaction = scriptContext.newRecord;

            if (_isDepositEligibleForJournalCreation(transaction, scriptContext)) {
                new journalBuilder.Instance()
                    .createVATCorrectionJournalAR(transaction)
                    .saveJournalReferenceOnDeposit();
            }
            else if (_isInvoiceEligibleForJournalReversalCheck(transaction, scriptContext)) {
                let journalIdForReversal = _getDepositJournalIdForReversal(transaction);

                if (journalIdForReversal) {
                    let invoiceDate = transaction.getValue('trandate');
                    new journalBuilder.Instance()
                        .reverseJournalEntry(journalIdForReversal, invoiceDate);
                }
            }
            else if (_isPrepaymentEligibleForJournalCreation(transaction, scriptContext)) {
                let scriptObj = runtime.getCurrentScript();
                let correctionJournalForm = scriptObj.getParameter(constants.SCRIPT_PARAMS.USER_EVENT_SCRIPT.CORRECTION_JOURNAL_FORM);
                new journalBuilder.Instance()
                    .createVATCorrectionJournalAP(transaction, correctionJournalForm)
                    .saveJournalReferenceOnPrepayment();
            }
            else if (_isVendorBillEligibleForJournalReversalCheck(transaction, scriptContext)) {
                let journalIdForReversal = _getPrepaymentJournalIdForReversal(transaction);
                log.debug('TEST', 'journalIdForReversal: ' + journalIdForReversal);

                if (journalIdForReversal) {
                    let vendorBillDate = transaction.getValue('trandate');
                    new journalBuilder.Instance()
                        .reverseJournalEntry(journalIdForReversal, vendorBillDate);
                }
            }

        }

        const _isDepositEligibleForJournalCreation = function (transaction, scriptContext) {
            return transaction.type === record.Type.CUSTOMER_DEPOSIT &&
                scriptContext.type !== scriptContext.UserEventType.DELETE &&
                transaction.getValue(constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CODE) &&
                !transaction.getValue(constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CORRECTION_JOURNAL);
        }

        const _isPrepaymentEligibleForJournalCreation = function (transaction, scriptContext) {
            return transaction.type === record.Type.VENDOR_PREPAYMENT &&
                scriptContext.type !== scriptContext.UserEventType.DELETE &&
                transaction.getValue('purchaseorder') &&
                transaction.getValue(constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CODE) &&
                !transaction.getValue(constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CORRECTION_JOURNAL);
        }

        const _isInvoiceEligibleForJournalReversalCheck = function (transaction, scriptContext) {
            let salesOrder = transaction.getValue('createdfrom');
            return transaction.type === record.Type.INVOICE && salesOrder &&
                scriptContext.type === scriptContext.UserEventType.CREATE;
        }

        const _isVendorBillEligibleForJournalReversalCheck = function (transaction, scriptContext) {
            return transaction.type === record.Type.VENDOR_BILL &&
                scriptContext.type === scriptContext.UserEventType.CREATE &&
                transactionDAO.getPurchaseOrderIDFromVendorBill(transaction.id);
        }

        const _getDepositJournalIdForReversal = function (transaction) {
            let salesOrder = transaction.getValue('createdfrom');
            let totalAmount = transaction.getValue('total');

            return transactionDAO.getDepositJournalForReversal(salesOrder, totalAmount);
        }

        const _getPrepaymentJournalIdForReversal = function (transaction) {
            let purchaseOrder = transactionDAO.getPurchaseOrderIDFromVendorBill(transaction.id);
            let totalAmount = transaction.getValue('total');

            return transactionDAO.getPrepaymentJournalForReversal(purchaseOrder, totalAmount);
        }

        return {afterSubmit}
});
