/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['../common/constants', '../dao/transaction'],

function(constants, transactionDAO) {

    /**
     * Validation function to be executed when record is saved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @returns {boolean} Return true if record is valid
     *
     * @since 2015.2
     */
    function saveRecord(scriptContext) {
        let transaction = scriptContext.currentRecord;
        let vendorPrepaymentId = transaction.getValue('vendorprepayment');
        let journalId = transactionDAO.getJournalIdFromVendorPrepayment(vendorPrepaymentId);

        if (journalId) {
            alert (constants.MESSAGES.VAT_CORRECTION_JOURNAL_EXISTS);
        }

        return true;
    }

    return {
        saveRecord: saveRecord
    };
    
});
