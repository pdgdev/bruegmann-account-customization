/**
 * @NApiVersion 2.1
 */
define([
        'N/record',
        'N/format',
        '../common/constants',
        '../dao/transaction',
    ],
    (
        record,
        format,
        constants,
        transactionDAO
    ) => {

        function JournalBuilder () {
            this.journalEntry = null;
            this.journalEntryId = null;
            this.customerDeposit = null;
            this.vendorPrepayment = null;
        }

        JournalBuilder.prototype.createVATCorrectionJournalAR = function (customerDeposit) {
            this.customerDeposit = customerDeposit;

            this.journalEntry = record.create({
                type: record.Type.JOURNAL_ENTRY,
                isDynamic: true,
            });

            let subsidiary = customerDeposit.getValue('subsidiary');
            let transactionDate = customerDeposit.getValue('trandate');
            let amount = customerDeposit.getValue('payment');
            let depositNumber = transactionDAO.getUpdatedTransactionNumber(customerDeposit.id);
            let memo = constants.VALUES.VAT_CORRECTION_MEMO_AR + depositNumber;
            let VATCode = customerDeposit.getValue(constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CODE);
            let account = transactionDAO.getCreditAccountFromDeposit(customerDeposit.id);

            this._setHeaderFields(subsidiary, transactionDate, memo);
            this._createJournalLines('AR', account, amount, VATCode);
            this._saveTransaction();

            return this;
        };

        JournalBuilder.prototype.createVATCorrectionJournalAP = function (vendorPrepayment, correctionJournalForm) {
            this.vendorPrepayment = vendorPrepayment;

            this.journalEntry = record.create({
                type: record.Type.JOURNAL_ENTRY,
                isDynamic: true,
                defaultValues: {
                    'customform': correctionJournalForm,
                },
            });

            let subsidiary = vendorPrepayment.getValue('subsidiary');
            let transactionDate = vendorPrepayment.getValue('trandate');
            let amount = vendorPrepayment.getValue('payment');
            let prepaymentNumber = transactionDAO.getUpdatedTransactionNumber(vendorPrepayment.id);
            let memo = constants.VALUES.VAT_CORRECTION_MEMO_AP + prepaymentNumber;
            let VATCode = vendorPrepayment.getValue(constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CODE);
            let account = transactionDAO.getDebitAccountFromPrepayment(vendorPrepayment.id);

            this._setHeaderFields(subsidiary, transactionDate, memo);
            this._createJournalLines('AP', account, amount, VATCode);
            this._saveTransaction();

            return this;
        };

        JournalBuilder.prototype.saveJournalReferenceOnDeposit = function () {
            let updateValues = {};
            updateValues[constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CORRECTION_JOURNAL] = this.journalEntryId;

            record.submitFields({
                type: record.Type.CUSTOMER_DEPOSIT,
                id: this.customerDeposit.id,
                values: updateValues,
            });

            return this;
        }

        JournalBuilder.prototype.saveJournalReferenceOnPrepayment = function () {
            let updateValues = {};
            updateValues[constants.CUSTOM_FIELDS.TRANSACTION_BODY.VAT_CORRECTION_JOURNAL] = this.journalEntryId;

            record.submitFields({
                type: record.Type.VENDOR_PREPAYMENT,
                id: this.vendorPrepayment.id,
                values: updateValues,
            });

            return this;
        }

        JournalBuilder.prototype.reverseJournalEntry = function (journalIdForReversal, date) {
            record.submitFields({
                type: record.Type.JOURNAL_ENTRY,
                id: journalIdForReversal,
                values: {'reversaldate': date},
            });

            log.debug('<< journalBuilder >>', 'Journal Entry #' + journalIdForReversal + ' has been reversed.');

            return this;
        };

        JournalBuilder.prototype._setHeaderFields = function (subsidiary, transactionDate, memo) {
            this.journalEntry.setValue({
                fieldId: 'subsidiary',
                value: subsidiary,
            });

            let formattedDate = format.parse({
                value: transactionDate,
                type: format.Type.DATE
            });

            this.journalEntry.setValue({
                fieldId: 'trandate',
                value: formattedDate,
            });

            this.journalEntry.setValue({
                fieldId: 'memo',
                value: memo
            });

            return this;
        }

        JournalBuilder.prototype._createJournalLines = function (type, account, amount, VATCode) {
            this.journalEntry.selectNewLine({
                sublistId: 'line',
            });

            this.journalEntry.setCurrentSublistValue({
                sublistId: 'line',
                fieldId: 'account',
                value: account,
            });

            this.journalEntry.setCurrentSublistValue({
                sublistId: 'line',
                fieldId: (type === 'AR') ? 'debit' : 'credit',
                value: amount,
            });

            this.journalEntry.commitLine({
                sublistId: 'line',
            });

            this.journalEntry.selectNewLine({
                sublistId: 'line',
            });

            this.journalEntry.setCurrentSublistValue({
                sublistId: 'line',
                fieldId: 'account',
                value: account,
            });

            this.journalEntry.setCurrentSublistValue({
                sublistId: 'line',
                fieldId: (type === 'AR') ? 'credit' : 'debit',
                value: amount,
            });

            this.journalEntry.setCurrentSublistValue({
                sublistId: 'line',
                fieldId: 'taxcode',
                value: VATCode,
            });

            this.journalEntry.setCurrentSublistValue({
                sublistId: 'line',
                fieldId: 'grossamt',
                value: amount,
            });

            this.journalEntry.commitLine({
                sublistId: 'line',
            });
        }

        JournalBuilder.prototype._saveTransaction = function () {
            this.journalEntryId = this.journalEntry.save();
            log.debug('<< journalBuilder >>', 'Journal Entry #' + this.journalEntryId + ' has been created.');
        };

        return {Instance: JournalBuilder};
    });