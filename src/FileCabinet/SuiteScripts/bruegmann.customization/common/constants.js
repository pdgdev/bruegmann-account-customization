define([], function () {
    return Object.freeze({
        CUSTOM_FIELDS: {
            TRANSACTION_BODY: {
                VAT_CODE: 'custbody_pdg_vat_code',
                VAT_CORRECTION_JOURNAL: 'custbody_pdg_vat_correction_je',
            },
            OTHER: {
                GERMAN_TAX_CODE: 'custrecord_pdg_german_tax_code',
            },
        },
        SCRIPT_PARAMS: {
            CLIENT_SCRIPT: {
                GERMAN_SUBSIDIARIES: 'custscript_pdg_german_subsidiaries',
            },
            USER_EVENT_SCRIPT: {
                CORRECTION_JOURNAL_FORM: 'custscript_pdg_correction_journal_form',
            },
        },
        VALUES: {
            VAT_CORRECTION_MEMO_AR: 'VAT correction Journal for Deposit ',
            VAT_CORRECTION_MEMO_AP: 'VAT correction Journal for Prepayment ',
        },
        MESSAGES: {
            VAT_CORRECTION_JOURNAL_EXISTS: 'A journal VAT correction is already linked to this transaction, kindly check the journal to make the necessary adjustments if needed.',
        },
    });
});